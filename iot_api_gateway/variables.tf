variable "region" {
  description = "The AWS region to create resources in."
  type        = string
  default     = "us-west-2"
}

variable "api_name" {
  description = "The name of the API Gateway."
  type        = string
}

variable "api_description" {
  description = "The description of the API Gateway."
  type        = string
  default     = "API Gateway for Lambda function"
}

variable "resource_path" {
  description = "The path part of the API Gateway resource."
  type        = string
}

variable "http_method" {
  description = "The HTTP method for the API Gateway method."
  type        = string
  default     = "POST"
}

variable "lambda_function_arn" {
  description = "The ARN of the Lambda function to trigger."
  type        = string
}

variable "stage_name" {
  description = "The stage name for the API Gateway deployment."
  type        = string
  default     = "prod"
}
