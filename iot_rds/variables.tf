variable "region" {
  description = "The AWS region to create resources in."
  type        = string
  default     = "us-west-2"
}

variable "allocated_storage" {
  description = "The allocated storage in gigabytes."
  type        = number
  default     = 20
}

variable "engine_version" {
  description = "The version of the PostgreSQL engine to use."
  type        = string
  default     = "15.5"
}

variable "instance_class" {
  description = "The instance class to use."
  type        = string
  default     = "db.t3.micro"
}

variable "db_name" {
  description = "The name of the database."
  type        = string
  default     = "iotdb"
}

variable "username" {
  description = "The username for the master DB user."
  type        = string
  default     = "iotapp"
}

variable "password" {
  description = "The password for the master DB user."
  type        = string
  default     = "i0T#202423"
  sensitive   = true
}

variable "subnet_ids" {
  description = "A list of subnet IDs for the DB subnet group."
  type        = list(string)
}

variable "vpc_id" {
  description = "The VPC ID where the RDS instance will be created."
  type        = string
}

variable "allowed_cidr_blocks" {
  description = "A list of CIDR blocks allowed to access the RDS instance."
  type        = list(string)
  default     = ["0.0.0.0/0"]
}

variable "multi_az" {
  description = "Specifies if the RDS instance is multi-AZ."
  type        = bool
  default     = false
}

variable "storage_type" {
  description = "The storage type to be associated with the RDS instance."
  type        = string
  default     = "gp2"
}

variable "publicly_accessible" {
  description = "Specifies if the RDS instance is publicly accessible."
  type        = bool
  default     = false
}

variable "skip_final_snapshot" {
  description = "Determines whether a final DB snapshot is created before the DB instance is deleted."
  type        = bool
  default     = true
}

variable "backup_retention_period" {
  description = "The number of days to retain backups for."
  type        = number
  default     = 7
}

variable "kms_key_arn" {
  description = "The ID of the KMS key to use for encryption."
  type        = string
}

variable "environment" {
  description = "The environment name (e.g., dev, prod)."
  type        = string
  default     = "iot"
}
