output "rds_endpoint" {
  description = "The endpoint of the RDS instance."
  value       = aws_db_instance.postgresql.endpoint
}

output "rds_instance_id" {
  description = "The ID of the RDS instance."
  value       = aws_db_instance.postgresql.id
}

output "rds_instance_address" {
  description = "The address of the RDS instance."
  value       = aws_db_instance.postgresql.address
}

output "rds_instance_port" {
  description = "The port of the RDS instance."
  value       = aws_db_instance.postgresql.port
}

output "rds_security_group_id" {
  description = "The ID of the security group associated with the RDS instance."
  value       = aws_security_group.rds_sg.id
}
