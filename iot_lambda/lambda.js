document.getElementById('upload').addEventListener('change', function(event) {
    const file = event.target.files[0];
    if (!file) return;
  
    const reader = new FileReader();
    reader.onload = function(e) {
      const img = new Image();
      img.src = e.target.result;
  
      img.onload = function() {
        const canvas = document.getElementById('canvas');
        const ctx = canvas.getContext('2d');
  
        // Set canvas dimensions to the image dimensions
        canvas.width = img.width;
        canvas.height = img.height;
  
        // Draw the image on the canvas
        ctx.drawImage(img, 0, 0);
  
        // Compress the image by setting the quality to 0.7 (70%)
        const compressedDataUrl = canvas.toDataURL('image/jpeg', 0.7);
  
        // Display the compressed image
        const compressedImgElement = document.getElementById('compressed-image');
        compressedImgElement.src = compressedDataUrl;
        compressedImgElement.style.display = 'block';
      }
    };
    reader.readAsDataURL(file);
  });
  