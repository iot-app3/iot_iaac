variable "region" {
  description = "The AWS region to create resources in."
  type        = string
  default     = "us-west-2"
}

variable "environment" {
  description = "The environment name (e.g., dev, prod)."
  type        = string
  default     = "iot"
}

variable "lambda_name" {
  description = "The name of the Lambda function."
  type        = string
}

variable "handler" {
  description = "The function entrypoint in your code."
  type        = string
}

variable "runtime" {
  description = "The runtime environment for the Lambda function."
  type        = string
}

variable "lambda_filename" {
  description = "The filename of the Lambda deployment package."
  type        = string
}


variable "s3_bucket" {
  description = "The name of the S3 bucket containing the Lambda deployment package."
  type        = string
}

variable "s3_key" {
  description = "The S3 key (path) to the Lambda deployment package."
  type        = string
}

variable "environment_variables" {
  description = "Environment variables for the Lambda function."
  type        = map(string)
  default     = {}
}
