variable "region" {
  description = "The AWS region to create resources in."
  type        = string
  default     = "us-west-2"
}

variable "ami" {
  description = "The AMI to use for the instance."
  type        = string
  default     = "ami-0c55b159cbfafe1f0"  # This is an example, update with your desired AMI
}

variable "instance_type" {
  description = "The type of instance to create."
  type        = string
  default     = "t2.micro"
}

variable "subnet_id" {
  description = "The subnet ID where the instance will be created."
  type        = string
}

variable "vpc_id" {
  description = "The VPC ID where the instance will be created."
  type        = string
}

variable "security_group_ids" {
  description = "The security group IDs to associate with the instance."
  type        = list(string)
  default     = []
}

variable "user_data" {
  description = "The user data to provide when launching the instance."
  type        = string
  default     = ""
}

variable "s3_iam_role" {
  description = "The user data to provide when launching the instance."
  type        = string
}

variable "environment" {
  description = "The environment name (e.g., dev, prod)."
  type        = string
  default     = "dev"
}
