variable "region" {
  description = "The AWS region to create resources in."
  type        = string
  default     = "us-west-2"
}

variable "key_name" {
  description = "The name of the KMS key."
  type        = string
  default     = "iot-kms-key"
}

variable "key_description" {
  description = "The description of the KMS key."
  type        = string
  default     = "KMS key for encrypting sensitive data"
}

variable "alias_name" {
  description = "The alias name for the KMS key."
  type        = string
  default     = "iot-key-alias"
}

variable "deletion_window_in_days" {
  description = "Specifies the number of days in the waiting period before AWS KMS deletes the KMS key."
  type        = number
  default     = 30
}

variable "enable_key_rotation" {
  description = "Specifies whether key rotation is enabled."
  type        = bool
  default     = true
}
