output "s3_bucket_name" {
  description = "The name of the S3 bucket."
  value       = aws_s3_bucket.this.bucket
}

output "iam_role_name" {
  description = "The name of the IAM role."
  value       = aws_iam_role.ec2_role.name
}

output "iam_role_arn" {
  description = "The ARN of the IAM role."
  value       = aws_iam_role.ec2_role.arn
}
