variable "region" {
  description = "The AWS region to create resources in."
  type        = string
  default     = "us-west-2"
}

variable "vpc_cidr" {
  description = "The CIDR block for the VPC."
  type        = string
  default     = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
  description = "The CIDR block for the public subnet."
  type        = string
  default     = "10.0.1.0/24"
}

variable "private_subnet_a_cidr" {
  description = "The CIDR block for the first private subnet."
  type        = string
  default     = "10.0.2.0/24"
}

variable "private_subnet_b_cidr" {
  description = "The CIDR block for the second private subnet."
  type        = string
  default     = "10.0.3.0/24"
}

variable "public_subnet_az" {
  description = "The availability zone for the public subnet."
  type        = string
  default     = "us-west-2a"
}

variable "private_subnet_a_az" {
  description = "The availability zone for the first private subnet."
  type        = string
  default     = "us-west-2b"
}

variable "private_subnet_b_az" {
  description = "The availability zone for the second private subnet."
  type        = string
  default     = "us-west-2c"
}
variable "environment" {
  description = "The environment name (e.g., dev, prod)."
  type        = string
  default     = "iot"
}
