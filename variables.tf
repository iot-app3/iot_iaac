variable "region" {
  description = "The AWS region to create resources in."
  type        = string
  default     = "us-west-2"
}

variable "vpc_cidr" {
  description = "The CIDR block for the VPC."
  type        = string
  default     = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
  description = "The CIDR block for the public subnet."
  type        = string
  default     = "10.0.1.0/24"
}

variable "private_subnet_a_cidr" {
  description = "The CIDR block for the first private subnet."
  type        = string
  default     = "10.0.2.0/24"
}

variable "private_subnet_b_cidr" {
  description = "The CIDR block for the second private subnet."
  type        = string
  default     = "10.0.3.0/24"
}



variable "public_subnet_az" {
  description = "The availability zone for the public subnet."
  type        = string
  default     = "us-west-2a"
}

variable "private_subnet_a_az" {
  description = "The availability zone for the first private subnet."
  type        = string
  default     = "us-west-2b"
}

variable "private_subnet_b_az" {
  description = "The availability zone for the second private subnet."
  type        = string
  default     = "us-west-2c"
}


variable "ami" {
  description = "The AMI to use for the instance."
  type        = string
  default     = "ami-0f7197c592205b389"  # This is an example, update with your desired AMI
}

variable "instance_type" {
  description = "The type of instance to create."
  type        = string
  default     = "t2.micro"
}

variable "bucket_name" {
  description = "The name of the S3 bucket."
  type        = string
  default = "iot-files-okhalaf"
}

variable "api_name" {
  description = "The name of the API Gateway."
  type        = string
  default     =  "iot-api"
}

variable "api_description" {
  description = "The description of the API Gateway."
  type        = string
  default     = "API Gateway for Lambda function"
}

variable "resource_path" {
  description = "The path part of the API Gateway resource."
  type        = string
}

variable "http_method" {
  description = "The HTTP method for the API Gateway method."
  type        = string
  default     = "POST"
}

variable "lambda_name" {
  description = "The name of the Lambda function."
  type        = string
}

variable "handler" {
  description = "The function entrypoint in your code."
  type        = string
}

variable "runtime" {
  description = "The runtime environment for the Lambda function."
  type        = string

}

variable "lambda_filename" {
  description = "The filename of the Lambda deployment package."
  type        = string
  default = "./iot_lambda/lambda.js"
}

variable "environment_variables" {
  description = "Environment variables for the Lambda function."
  type        = map(string)
  default     = {}
}

variable "stage_name" {
  description = "The stage name for the API Gateway deployment."
  type        = string
  default     = "prod"
}

variable "environment" {
  description = "The environment name (e.g., dev, prod)."
  type        = string
  default     = "iot"
}
