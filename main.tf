provider "aws" {
  region = var.region
}

module "vpc" {
  source = "./iot_vpc"
  region             = var.region
  vpc_cidr           = var.vpc_cidr
  public_subnet_cidr = var.public_subnet_cidr
  private_subnet_a_cidr = var.private_subnet_a_cidr
  private_subnet_b_cidr = var.private_subnet_b_cidr
  public_subnet_az   = var.public_subnet_az
  private_subnet_a_az  = var.private_subnet_a_az
  private_subnet_b_az  = var.private_subnet_b_az
  environment        = var.environment
}

module "ec2" {
  source = "./iot_ec2"

  region            = var.region
  ami               = var.ami
  instance_type     = var.instance_type
  subnet_id         = module.vpc.private_subnet_a_id
  vpc_id            = module.vpc.vpc_id
  #security_group_ids = [module.vpc.security_group_id]
  environment       = var.environment
  s3_iam_role       = module.s3_iam.iam_role_name     
}

module "kms" {
  source = "./iot_kms"
  region            = var.region
}

module "rds" {
  source = "./iot_rds"
  region            = var.region
  #subnet_ids         = split(",", module.vpc.private_subnet_id)
  subnet_ids  = [module.vpc.private_subnet_a_id, module.vpc.private_subnet_b_id]
  vpc_id            = module.vpc.vpc_id
  kms_key_arn      =   module.kms.kms_key_arn
}

module "s3_iam" {
  source = "./iot_s3_iam"

  region      = var.region
  bucket_name = var.bucket_name
  environment = var.environment
}

# module "api_gateway" {
#   source               = "./iot_api_gateway"
#   region               = var.region
#   api_name             = var.api_name
#   api_description      = var.api_description
#   resource_path        = var.resource_path
#   http_method          = var.http_method
#   lambda_function_arn  = module.lambda.lambda_function_arn
#   stage_name           = var.stage_name
# }

# module "lambda" {
#   source                = "./iot_lambda"
#   region                = var.region
#   environment           = var.environment
#   lambda_name           = var.lambda_name
#   handler               = var.handler
#   runtime               = var.runtime
#   lambda_filename       = var.lambda_filename
#   environment_variables = var.environment_variables
# }



output "ec2_instance_id" {
  value = module.ec2.instance_id
}

output "ec2_instance_public_ip" {
  value = module.ec2.instance_public_ip
}

output "ec2_instance_private_ip" {
  value = module.ec2.instance_private_ip
}
